%%
   
%package fr.uha.hassenforder.mdpi_parser
%class Lexer
%line
%column
%cup

%%
[ \t\f]					{ }
\r\n|\r|\n				{ }
MDPI11articletype		{ return getSymbolFactory().newSymbol ("MDPI11articletype", Sym.MDPI11articletype, yyline, yycolumn); }
MDPI12title				{ return getSymbolFactory().newSymbol ("MDPI12title", Sym.MDPI12title, yyline, yycolumn); }
MDPI13authornames		{ return getSymbolFactory().newSymbol ("MDPI13authornames", Sym.MDPI13authornames, yyline, yycolumn); }
MDPI14history			{ return getSymbolFactory().newSymbol ("MDPI14history", Sym.MDPI14history, yyline, yycolumn); }
MDPI16affiliation		{ return getSymbolFactory().newSymbol ("MDPI16affiliation", Sym.MDPI16affiliation, yyline, yycolumn); }
MDPI17abstract			{ return getSymbolFactory().newSymbol ("MDPI17abstract", Sym.MDPI17abstract, yyline, yycolumn); }
MDPI18keywords			{ return getSymbolFactory().newSymbol ("MDPI18keywords", Sym.MDPI18keywords, yyline, yycolumn); }
MDPI19line				{ return getSymbolFactory().newSymbol ("MDPI19line", Sym.MDPI19line, yyline, yycolumn); }

MDPI21heading1			{ return getSymbolFactory().newSymbol ("MDPI21heading1", Sym.MDPI21heading1, yyline, yycolumn); }
MDPI22heading2			{ return getSymbolFactory().newSymbol ("MDPI22heading2", Sym.MDPI22heading2, yyline, yycolumn); }
MDPI23heading3			{ return getSymbolFactory().newSymbol ("MDPI23heading3", Sym.MDPI23heading3, yyline, yycolumn); }

MDPI31text				{ return getSymbolFactory().newSymbol ("MDPI31text", Sym.MDPI31text, yyline, yycolumn); }
MDPI39equation			{ return getSymbolFactory().newSymbol ("MDPI39equation", Sym.MDPI39equation, yyline, yycolumn); }
MDPI3aequationnumber	{ return getSymbolFactory().newSymbol ("MDPI3aequationnumber", Sym.MDPI3aequationnumber, yyline, yycolumn); }
   
MDPI41tablecaption		{ return getSymbolFactory().newSymbol ("MDPI41tablecaption", Sym.MDPI41tablecaption, yyline, yycolumn); }
MDPI42tablebody			{ return getSymbolFactory().newSymbol ("MDPI42tablebody", Sym.MDPI42tablebody, yyline, yycolumn); }
MDPI43tablefooter		{ return getSymbolFactory().newSymbol ("MDPI43tablefooter", Sym.MDPI43tablefooter, yyline, yycolumn); }

MDPI51figurecaption		{ return getSymbolFactory().newSymbol ("MDPI51figurecaption", Sym.MDPI51figurecaption, yyline, yycolumn); }
MDPI52figure			{ return getSymbolFactory().newSymbol ("MDPI52figure", Sym.MDPI52figure, yyline, yycolumn); }

MDPI62Acknowledgments	{ return getSymbolFactory().newSymbol ("MDPI62Acknowledgments", Sym.MDPI62Acknowledgments, yyline, yycolumn); }

MDPI71References		{ return getSymbolFactory().newSymbol ("MDPI71References", Sym.MDPI71References, yyline, yycolumn); }

TABLE_BEGIN				{ return getSymbolFactory().newSymbol ("TABLE_BEGIN", Sym.TABLE_BEGIN, yyline, yycolumn); }
TABLE_END				{ return getSymbolFactory().newSymbol ("TABLE_END", Sym.TABLE_END, yyline, yycolumn); }

PARAGRAPH				{ return getSymbolFactory().newSymbol ("PARAGRAPH", Sym.PARAGRAPH, yyline, yycolumn); }

[^]                     { throw new Error("Illegal character <"+yytext()+">"); }
