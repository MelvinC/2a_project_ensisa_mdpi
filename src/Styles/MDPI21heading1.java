package Styles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MDPI21heading1 {
	
	private static Pattern pattern = Pattern.compile("^\\s*[0-9]+\\. ");

	public static boolean match(String s) {
		Matcher matcher = pattern.matcher(s);
		return matcher.find();
	}
}
