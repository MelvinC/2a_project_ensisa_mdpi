package Styles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MDPI18keywords {
	
	private static Pattern pattern = Pattern.compile("^*[kK]eywords: ");

	public static boolean match(String s) {
		Matcher matcher = pattern.matcher(s);
		return matcher.find();
	}
}
