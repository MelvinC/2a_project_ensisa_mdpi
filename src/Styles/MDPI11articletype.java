package Styles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MDPI11articletype {
	
	private static Pattern pattern = Pattern.compile("[Aa]rticle");

	public static boolean match(String s) {
		Matcher matcher = pattern.matcher(s);
		return matcher.find();
	}
}
