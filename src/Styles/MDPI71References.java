package Styles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MDPI71References {
	
	private static Pattern pattern = Pattern.compile("^ *[0-9]+\\. {4}");

	public static boolean match(String s) {
		Matcher matcher = pattern.matcher(s);
		return matcher.find();
	}
}
