package Styles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MDPI62Acknowledgments {
	
	private static Pattern pattern = Pattern.compile("^ *([a-zA-Z]* *){3}[a-zA-Z]*: ");

	public static boolean match(String s) {
		Matcher matcher = pattern.matcher(s);
		return matcher.find();
	}
}
