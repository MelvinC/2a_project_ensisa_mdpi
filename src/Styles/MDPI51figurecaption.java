package Styles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MDPI51figurecaption {
	
	private static Pattern pattern = Pattern.compile("^ *[Tt]able [0-9]*\\. ");

	public static boolean match(String s) {
		Matcher matcher = pattern.matcher(s);
		return matcher.find();
	}
}
