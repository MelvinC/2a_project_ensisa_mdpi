package Styles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MDPI17abstract {
	
	private static Pattern pattern = Pattern.compile("^\\s*[Aa]bstract:");

	public static boolean match(String s) {
		Matcher matcher = pattern.matcher(s);
		return matcher.find();
	}
}
