package fr.uha.hassenforder.mdpi_parser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import Styles.EOF;
import Styles.MDPI11articletype;
import Styles.MDPI12title;
import Styles.MDPI13authornames;
import Styles.MDPI14history;
import Styles.MDPI16affiliation;
import Styles.MDPI17abstract;
import Styles.MDPI18keywords;
import Styles.MDPI19line;
import Styles.MDPI21heading1;
import Styles.MDPI22heading2;
import Styles.MDPI23heading3;
import Styles.MDPI31text;
import Styles.MDPI39equation;
import Styles.MDPI3aequationnumber;
import Styles.MDPI41tablecaption;
import Styles.MDPI42tablebody;
import Styles.MDPI43tablefooter;
import Styles.MDPI51figurecaption;
import Styles.MDPI52figure;
import Styles.MDPI62Acknowledgments;
import Styles.MDPI71References;
import Styles.PARAGRAPH;
import Styles.TABLE_BEGIN;
import Styles.TABLE_END;
import Styles.error;
import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.Symbol;

public class FlexLike implements java_cup.runtime.Scanner{
	
	private String name;
	private ComplexSymbolFactory sf;
	private Map<String, Integer> map;
	private List<String> ligne;
	private int position;

	public FlexLike(String name, ComplexSymbolFactory sf) {
		this.name = name;
		this.sf = sf;
		this.map = new HashMap<String, Integer>();
		this.map.put("MDPI12title", Sym.MDPI12title);
		this.map.put("PARAGRAPH", Sym.PARAGRAPH);
		this.map.put("MDPI31text", Sym.MDPI31text);
		this.map.put("MDPI51figurecaption", Sym.MDPI51figurecaption);
		this.map.put("TABLE_BEGIN", Sym.TABLE_BEGIN);
		this.map.put("MDPI23heading3", Sym.MDPI23heading3);
		this.map.put("MDPI18keywords", Sym.MDPI18keywords);
		this.map.put("MDPI16affiliation", Sym.MDPI16affiliation);
		this.map.put("MDPI42tablebody", Sym.MDPI42tablebody);
		this.map.put("MDPI39equation", Sym.MDPI39equation);
		this.map.put("MDPI43tablefooter", Sym.MDPI43tablefooter);
		this.map.put("MDPI62Acknowledgments", Sym.MDPI62Acknowledgments);
		this.map.put("MDPI11articletype", Sym.MDPI11articletype);
		this.map.put("MDPI13authornames", Sym.MDPI13authornames);
		this.map.put("MDPI14history", Sym.MDPI14history);
		this.map.put("MDPI71References", Sym.MDPI71References);
		this.map.put("EOF", Sym.EOF);
		this.map.put("MDPI17abstract", Sym.MDPI17abstract);
		this.map.put("error", Sym.error);
		this.map.put("TABLE_END", Sym.TABLE_END);
		this.map.put("MDPI41tablecaption", Sym.MDPI41tablecaption);
		this.map.put("MDPI52figure", Sym.MDPI52figure);
		this.map.put("MDPI3aequationnumber", Sym.MDPI3aequationnumber);
		this.map.put("MDPI19line", Sym.MDPI19line);
		this.map.put("MDPI22heading2", Sym.MDPI22heading2);
		this.map.put("MDPI21heading1", Sym.MDPI21heading1);
		
		this.ligne = new LinkedList<String>();
		this.position = 0;
		run();
	}
	
	public void run() {
		try {
			FileReader r = new FileReader(this.name);
			LineNumberReader lr = new LineNumberReader(r);
			String line;
			try {
				line = lr.readLine();
				while(line != null) {
					this.ligne.add(line);
					line=lr.readLine();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Symbol next_token() throws Exception {
		Symbol result = null;
		if(position >= ligne.size()) {
			return result;
		}
		String s = ligne.get(this.position);
		this.position++;
		String[] st = s.split(";");
		if(this.map.containsKey(st[0])) {
			result = this.sf.newSymbol (st[0], map.get(st[0]));
		}
		else {
			if(st.length<2) {
				System.out.println("erreur ligne "+position);
				return result;
			}
			String[] possibilities = st[2].split(",");
			LinkedList<Symbol> matches = new LinkedList<Symbol>();
			for(int i = 0; i<possibilities.length; i++) {
				matches.add(this.verify(st[1], possibilities[i]));
			}
			for(Symbol sym : matches) {
				if(sym != null) {
					result=sym;
					break;
				}
			}
			if(result == null) {
				System.out.println("erreur ligne "+position);
			}
		}
		return result;
	}
	
	public Symbol verify(String s1, String s2) {
		Symbol result = null;
		switch (s2.substring(1)) { //on enl�ve l'espace devant le string !
		case "MDPI12title":
			if(MDPI12title.match(s1)) {
				result = this.sf.newSymbol("MDPI12title", Sym.MDPI12title);
			}
			break;
		case "PARAGRAPH":
			if(PARAGRAPH.match(s1)) {
				result = this.sf.newSymbol("PARAGRAPH", Sym.PARAGRAPH);
			}
			break;
		case "MDPI31text":
			if(MDPI31text.match(s1)) {
				result = this.sf.newSymbol("MDPI31text", Sym.MDPI31text);
			}
			break;
		case "MDPI51figurecaption":
			if(MDPI51figurecaption.match(s1)) {
				result = this.sf.newSymbol("MDPI51figurecaption", Sym.MDPI51figurecaption);
			}
			break;
		case "TABLE_BEGIN":
			if(TABLE_BEGIN.match(s1)) {
				result = this.sf.newSymbol("TABLE_BEGIN", Sym.TABLE_BEGIN);
			}
			break;
		case "MDPI23heading3":
			if(MDPI23heading3.match(s1)) {
				result = this.sf.newSymbol("MDPI23heading3", Sym.MDPI23heading3);
			}
			break;
		case "MDPI18keywords":
			if(MDPI18keywords.match(s1)) {
				result = this.sf.newSymbol("MDPI18keywords", Sym.MDPI18keywords);
			}
			break;
		case "MDPI16affiliation":
			if(MDPI16affiliation.match(s1)) {
				result = this.sf.newSymbol("MDPI16affiliation", Sym.MDPI16affiliation);
			}
			break;
		case "MDPI42tablebody":
			if(MDPI42tablebody.match(s1)) {
				result = this.sf.newSymbol("MDPI42tablebody", Sym.MDPI42tablebody);
			}
			break;
		case "MDPI39equation":
			if(MDPI39equation.match(s1)) {
				result = this.sf.newSymbol("MDPI39equation", Sym.MDPI39equation);
			}
			break;
		case "MDPI43tablefooter":
			if(MDPI43tablefooter.match(s1)) {
				result = this.sf.newSymbol("MDPI43tablefooter", Sym.MDPI43tablefooter);
			}
			break;
		case "MDPI62Acknowledgments":
			if(MDPI62Acknowledgments.match(s1)) {
				result = this.sf.newSymbol("MDPI62Acknowledgments", Sym.MDPI62Acknowledgments);
			}
			break;
		case "MDPI11articletype":
			if(MDPI11articletype.match(s1)) {
				result = this.sf.newSymbol("MDPI11articletype", Sym.MDPI11articletype);
			}
			break;
		case "MDPI13authornames":
			if(MDPI13authornames.match(s1)) {
				result = this.sf.newSymbol("MDPI13authornames", Sym.MDPI13authornames);
			}
			break;
		case "MDPI14history":
			if(MDPI14history.match(s1)) {
				result = this.sf.newSymbol("MDPI14history", Sym.MDPI14history);
			}
			break;
		case "MDPI71References":
			if(MDPI71References.match(s1)) {
				result = this.sf.newSymbol("MDPI71References", Sym.MDPI71References);
			}
			break;
		case "EOF":
			if(EOF.match(s1)) {
				result = this.sf.newSymbol("EOF", Sym.EOF);
			}
			break;
		case "MDPI17abstract":
			if(MDPI17abstract.match(s1)) {
				result = this.sf.newSymbol("MDPI17abstract", Sym.MDPI17abstract);
			}
			break;
		case "error":
			if(error.match(s1)) {
				result = this.sf.newSymbol("error", Sym.error);
			}
			break;
		case "TABLE_END":
			if(TABLE_END.match(s1)) {
				result = this.sf.newSymbol("TABLE_END", Sym.TABLE_END);
			}
			break;
		case "MDPI41tablecaption":
			if(MDPI41tablecaption.match(s1)) {
				result = this.sf.newSymbol("MDPI41tablecaption", Sym.MDPI41tablecaption);
			}
			break;
		case "MDPI52figure":
			if(MDPI52figure.match(s1)) {
				result = this.sf.newSymbol("MDPI52figure", Sym.MDPI52figure);
			}
			break;
		case "MDPI3aequationnumber":
			if(MDPI3aequationnumber.match(s1)) {
				result = this.sf.newSymbol("MDPI3aequationnumber", Sym.MDPI3aequationnumber);
			}
			break;
		case "MDPI19line":
			if(MDPI19line.match(s1)) {
				result = this.sf.newSymbol("MDPI19line", Sym.MDPI19line);
			}
			break;
		case "MDPI22heading2":
			if(MDPI22heading2.match(s1)) {
				result = this.sf.newSymbol("MDPI22heading2", Sym.MDPI22heading2);
			}
			break;
		case "MDPI21heading1":
			if(MDPI21heading1.match(s1)) {
				result = this.sf.newSymbol("MDPI21heading1", Sym.MDPI21heading1);
			}
			break;
		default:
			break;
		}
		return result;
	}
}
